package org.xploration.team3.platform;

import java.util.HashSet;
import java.util.Set;

import org.xploration.ontology.RegistrationRequest;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;
import org.xploration.team3.common.Constants;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class RegistrationDesk extends Agent {

	private static final long serialVersionUID = -4544639492632411003L;
	private static final Ontology ontology = XplorationOntology.getInstance();
	private Codec codec = new SLCodec();
	private Set<Team> registeredTeams;

	public void setup() {

		System.out.println(getLocalName() + ": has entered into the system");
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);
		registeredTeams = new HashSet<>();
		try {
			// Creates its own description
			DFAgentDescription dfd = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setName(this.getName());
			sd.setType(Constants.REGISTRATIONDESK);
			dfd.addServices(sd);
			// Registers its description in the DF
			DFService.register(this, dfd);
			System.out.println(getLocalName() + ": registered in the DF");
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		addBehaviour(addRegistrationListenerBehaviour());
	}

	private Behaviour addRegistrationListenerBehaviour() {

		return new CyclicBehaviour(this) {

			private static final long serialVersionUID = 6514579335013817440L;

			@Override
			public void action() {

				// Waits for estimation requests
				ACLMessage msg = blockingReceive(MessageTemplate.and(MessageTemplate.MatchLanguage(codec.getName()),
						MessageTemplate.and(MessageTemplate.MatchOntology(ontology.getName()),
								MessageTemplate.MatchPerformative(ACLMessage.REQUEST))));
				if (msg != null) {
					// If an REGISTRATION request arrives (type REQUEST)
					// it answers with the REFUSE, AGREE or NU

					// The ContentManager transforms the message content
					// (string)
					// in java objects
					ContentElement ce;
					try {
						ce = getContentManager().extractContent(msg);

						// We expect an action inside the message
						if (ce instanceof Action) {
							Action agAction = (Action) ce;
							Concept conc = agAction.getAction();
							// If the action is RegistrationRequest...
							if (conc instanceof RegistrationRequest) {
								AID fromAgent = msg.getSender();
								System.out.println(myAgent.getLocalName() + ": received registration request from "
										+ (msg.getSender()).getLocalName());
								Team requestorTeam = ((RegistrationRequest) conc).getTeam();
								System.out.println(
										myAgent.getLocalName() + ": registration request for " + requestorTeam);

								ACLMessage reply = msg.createReply();

								// An ack of registration request is sent
								reply.setLanguage(codec.getName());
								reply.setOntology(ontology.getName());
								if (!canRegister()) {
									reply.setPerformative(ACLMessage.AGREE);
									// The ContentManager transforms the java
									// objects in strings
									myAgent.send(reply);
									System.out.println(myAgent.getLocalName() + ": registration request ack sent");

									if (registeredTeams.add(requestorTeam)) { // only
																				// adds
																				// if
																				// it
																				// doesn't
																				// contain
																				// team
																				// yet
										// team already registered
										ACLMessage finalMsg = new ACLMessage(ACLMessage.FAILURE);
										finalMsg.addReceiver(fromAgent);
										finalMsg.setLanguage(codec.getName());
										finalMsg.setOntology(ontology.getName());
										myAgent.send(finalMsg);
										System.out.println(
												myAgent.getLocalName() + ": failure registration response sent");
									} else {
										// team hasn't registered yet
										ACLMessage finalMsg = new ACLMessage(ACLMessage.INFORM);
										finalMsg.addReceiver(fromAgent);
										finalMsg.setLanguage(codec.getName());
										finalMsg.setOntology(ontology.getName());
										myAgent.send(finalMsg);
										System.out.println(
												myAgent.getLocalName() + ": positive registration response sent");
									}
								} else {
									reply.setPerformative(ACLMessage.REFUSE);
									myAgent.send(reply);
									System.out.println(myAgent.getLocalName() + ": registration too late, refuse sent");
								}

							} else {
								throw new NotUnderstoodException(msg);
							}
						} else {
							throw new NotUnderstoodException(msg);
						}
					} catch (CodecException | OntologyException | NotUnderstoodException e) {
						e.printStackTrace();
						AID fromAgent = msg.getSender();
						ACLMessage reply = msg.createReply();
						reply.setLanguage(codec.getName());
						reply.setOntology(ontology.getName());
						reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
						myAgent.send(reply);
						System.out.println(myAgent.getLocalName() + ": NOT UNDERSTOOD sent");
					}
				}
			}

		};
	}

	@Override
	protected void takeDown() {
		// Deregister from the yellow pages
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}

		// Printout a dismissal message
		System.out.println("RegistrationDesk agent " + getAID().getName() + " terminating.");
	}

	private boolean canRegister() {
		// TODO: add logic to determine when agents can register
		return true;
	}

}
